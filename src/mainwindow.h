#ifndef MAIN_WINDOW
#define MAIN_WINDOW

#include <functional>
#include <iostream>
#include <map>
#include <thread>
#include <vector>

#include <QMainWindow>
#include <QTextEdit>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

class UIUpdater;
class OperationStatus;
class FileProcessor;

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    MainWindow(const std::shared_ptr<UIUpdater>& updater, const std::shared_ptr<FileProcessor>& processor,
               QWidget* parent = 0) noexcept;
    void retranslateUi();

  private:
    // widgets of mainwindow
    std::map<int, std::pair<std::shared_ptr<QProgressBar>, std::shared_ptr<QLabel>>> m_progress_bars_results;

    std::vector<std::unique_ptr<QTextEdit>> m_monitors;

    std::unique_ptr<QTextEdit> m_n_threads_input;
    std::unique_ptr<QTextEdit> m_n_file_divisions_input;
    std::unique_ptr<QTextEdit> m_file_input;

    std::unique_ptr<QVBoxLayout> m_left_layout;
    std::unique_ptr<QPushButton> m_browse;
    std::unique_ptr<QPushButton> m_process;

    std::shared_ptr<UIUpdater> m_updater;

    // file processor
    std::shared_ptr<FileProcessor> m_processor;

    void clearLayout(QLayout* layout, const bool deleteWidgets) noexcept;

  public slots:
    void file_browse() noexcept;
    void process() noexcept;
    void new_calculation_slot(const int n) noexcept;
    void set_progress_slot(const int n, const int progress) noexcept;
    void set_result_slot(const int n, const int result) noexcept;
    void show_message_slot(const std::string& message, const std::string& status) noexcept;
};

#endif // MAIN_WINDOW
