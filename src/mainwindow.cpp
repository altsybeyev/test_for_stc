#include <boost/thread/future.hpp>

#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMessageBox>

#include "fileprocessor.h"
#include "mainwindow.h"
#include "uiupdater.h"

MainWindow::MainWindow(const std::shared_ptr<UIUpdater>& updater, const std::shared_ptr<FileProcessor>& processor,
                       QWidget* parent) noexcept
    : QMainWindow(parent), m_left_layout(new QVBoxLayout()), m_n_threads_input(new QTextEdit()),
      m_n_file_divisions_input(new QTextEdit()), m_file_input(new QTextEdit()), m_browse(new QPushButton("Brouse")),
      m_process(new QPushButton("Run")), m_updater(updater), m_processor(processor)
{
    resize(QDesktopWidget().availableGeometry(this).size() * 0.4);

    auto right_layout = new QVBoxLayout();

    m_left_layout->setAlignment(Qt::AlignTop);
    right_layout->setAlignment(Qt::AlignTop);

    m_n_threads_input->setMaximumSize(QSize(50, 30));
    m_n_file_divisions_input->setMaximumSize(QSize(250, 30));
    m_file_input->setMaximumSize(QSize(16777215, 50));
    m_process->setMaximumSize(QSize(60, 40));
    m_browse->setMaximumSize(QSize(60, 40));

    m_n_file_divisions_input->setText("150000000");
    m_n_threads_input->setText("1");

    auto horizontalLayout = new QHBoxLayout();

    auto centralWidget = new QWidget();
    auto left_widget   = new QWidget();
    auto right_widget  = new QWidget();

    centralWidget->setLayout(horizontalLayout);

    this->setCentralWidget(centralWidget);

    horizontalLayout->addWidget(left_widget);
    horizontalLayout->addWidget(right_widget);

    right_widget->setMaximumSize(QSize(width() / 2.0, height()));
    left_widget->setMaximumSize(QSize(width() / 2.0, height()));

    left_widget->setLayout(m_left_layout.get());

    right_widget->setLayout(right_layout);

    right_layout->addWidget(new QLabel("File"));
    right_layout->addWidget(m_file_input.get());
    right_layout->addWidget(m_browse.get());

    right_layout->addWidget(new QLabel("N threads"));
    right_layout->addWidget(m_n_threads_input.get());

    right_layout->addWidget(new QLabel("M size, bytes"));
    right_layout->addWidget(m_n_file_divisions_input.get());
    right_layout->addWidget(m_process.get());

    connect(m_browse.get(), SIGNAL(clicked()), this, SLOT(file_browse()));

    connect(m_updater.get(), SIGNAL(update_status_signal(const OperationStatus&)), this,
            SLOT(process_operation_status_slot(const OperationStatus&)));

    connect(m_updater.get(), SIGNAL(new_calculation_signal(const int)), this, SLOT(new_calculation_slot(const int)));
    connect(m_updater.get(), SIGNAL(set_progress_signal(const int, const int)), this,
            SLOT(set_progress_slot(const int, const int)));

    connect(m_updater.get(), SIGNAL(show_message_signal(const std::string&, const std::string&)), this,
            SLOT(show_message_slot(const std::string&, const std::string&)));

    connect(m_updater.get(), SIGNAL(set_result_signal(const int, const int)), this,
            SLOT(set_result_slot(const int, const int)));

    connect(m_process.get(), SIGNAL(clicked()), this, SLOT(process()));
}

void MainWindow::file_browse() noexcept
{
    std::string errorMessage;

    auto fileName = QFileDialog::getOpenFileName(this, "Open File", "", "");

    if (!fileName.size())
        return;

    m_file_input->setText(fileName);
}

void MainWindow::clearLayout(QLayout* layout, const bool deleteWidgets) noexcept
{
    while (QLayoutItem* item = layout->takeAt(0))
    {
        if (deleteWidgets)
        {
            if (QWidget* widget = item->widget())
                delete widget;
        }
        if (QLayout* childLayout = item->layout())
            clearLayout(childLayout, deleteWidgets);
        delete item;
    }
}

void MainWindow::new_calculation_slot(const int n) noexcept
{
    clearLayout(m_left_layout.get(), false);
    m_progress_bars_results.clear();

    for (int i = 0; i < n; i++)
    {
        m_progress_bars_results.emplace(
            i, std::pair<std::shared_ptr<QProgressBar>, std::shared_ptr<QLabel>>(new QProgressBar(), new QLabel()));
        auto label = m_progress_bars_results[i].second.get();
        label->setText("Result: ");
        m_left_layout->addWidget(label);
        m_left_layout->addWidget(m_progress_bars_results[i].first.get());
    }
}

void MainWindow::show_message_slot(const std::string& message, const std::string& status) noexcept
{
    if (!message.empty())
    {
        QMessageBox messageBox;
        if (status == "Error")
        {
            messageBox.critical(0, status.c_str(), message.c_str());
        }
        else
        {
            messageBox.information(0, status.c_str(), message.c_str());
        }
        messageBox.setFixedSize(500, 200);
    }
}

void MainWindow::set_progress_slot(const int n, const int progress) noexcept
{
    auto it = m_progress_bars_results.find(n);
    if (it != m_progress_bars_results.end())
    {
        it->second.first->setValue(progress);
    }
}

void MainWindow::set_result_slot(const int n, const int result) noexcept
{
    auto it = m_progress_bars_results.find(n);
    if (it != m_progress_bars_results.end())
    {
        it->second.second->setText("Result: " + QString::number(result));
    }
}

void MainWindow::process() noexcept
{

    boost::async(boost::launch::async, [this]() {
        m_processor->process(m_file_input->toPlainText().toStdString(), m_n_threads_input->toPlainText().toStdString(),
                             m_n_file_divisions_input->toPlainText().toStdString());
    });
}
