#include <QApplication>

#include "fileprocessor.h"
#include "mainwindow.h"
#include "uiupdater.h"

Q_DECLARE_METATYPE(std::string)

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);

    qRegisterMetaType<std::string>();

    auto updater = std::make_shared<UIUpdater>();

    auto processor = std::make_shared<FileProcessor>(updater);

    MainWindow w(updater, processor);

    w.show();

    return a.exec();
}
