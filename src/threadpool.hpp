#ifndef COMM_TOOLS_THREADPOOL_HPP
#define COMM_TOOLS_THREADPOOL_HPP

#include <list>
#include <queue>

#include <boost/thread.hpp>

namespace commtools
{
class Worker;
class WorkerSharedQueue;
class ThreadPullSharedQueue;
class ThreadPullDifferentQueue;
template <class Tworker> class ThreadPoolT;

class ThreadPool
{
  public:
#define MAKE_ASYNC_MASTER_FUNCTION(inner_rule)                                                                         \
    auto promise = std::make_shared<boost::promise<typename std::result_of<Function(Args...)>::type>>();               \
    auto future  = promise->get_future();                                                                              \
                                                                                                                       \
    auto rfn = std::bind(f, std::forward<Args>(args)...);                                                              \
                                                                                                                       \
    auto fn = [=]() -> void {                                                                                          \
        try                                                                                                            \
        {                                                                                                              \
            inner_rule                                                                                                 \
        }                                                                                                              \
        catch (...)                                                                                                    \
        {                                                                                                              \
            promise->set_exception(std::current_exception());                                                          \
        }                                                                                                              \
    };                                                                                                                 \
                                                                                                                       \
    appendFn(fn, tag);                                                                                                 \
                                                                                                                       \
    return future;

    template <class Function, class... Args>
    boost::future<void> run_async_master(const std::string& tag, Function&& f, Args&&... args)
    {
        MAKE_ASYNC_MASTER_FUNCTION(rfn(); promise->set_value();)
    }

    template <class Function, class... Args>
    boost::future<typename std::enable_if<!std::is_void<typename std::result_of<Function(Args...)>::type>::value,
                                          typename std::result_of<Function(Args...)>::type>::type>
    run_async_master(const std::string& tag, Function&& f, Args&&... args)
    {
        MAKE_ASYNC_MASTER_FUNCTION(promise.set_value(rfn());)
    }

    template <class Function, class... Args>
    boost::future<typename std::result_of<Function(Args...)>::type> run_async_slave(Function&& f, Args&&... args)
    {
        return run_async_master(get_parent_tag(), f, std::forward<Args>(args)...);
    }

    static ThreadPool& get(const int threads, const std::vector<std::string> m_observers = {},
                           const bool is_common = false);
    static ThreadPool& get();

    virtual size_t size() const noexcept         = 0;
    virtual int    n_tasks_in_process() noexcept = 0;

    virtual size_t free_workers() const noexcept = 0;

    virtual void wait()      = 0;
    virtual ~ThreadPool()    = default;
    virtual void terminate() = 0;

    virtual void wait_for_one_task_finishing() noexcept = 0;

  private:
    static std::unique_ptr<ThreadPool> m_instance;

    virtual std::string get_parent_tag() const noexcept = 0;

    ThreadPool(ThreadPool const&) = delete;
    ThreadPool(ThreadPool&&)      = delete;
    ThreadPool& operator=(ThreadPool const&) = delete;
    ThreadPool& operator=(ThreadPool&&) = delete;
    ThreadPool()                        = default;

    virtual void appendFn(const std::function<void()>& fn, const std::string& tag) = 0;

    friend class ThreadPullSharedQueue;
    friend class ThreadPullDifferentQueue;
    friend class ThreadPoolT<Worker>;
    friend class ThreadPoolT<WorkerSharedQueue>;
};

template <class Tworker> class ThreadPoolT : public ThreadPool
{
  public:
    virtual ~ThreadPoolT() = default;

  protected:
    ThreadPoolT(const std::vector<std::string>& observers) noexcept : m_observers(observers)
    {
    }

    template <typename... _Args> void init(const int threads, _Args&&... __args)
    {
        m_workers.clear();
        if (threads < 1)
        {
#ifndef DONT_USE_BOOSTLOG
            BL_WARNING_CH("THPOOL") << "ThreadPool::ThreadPool: threads < 1. Force set threads = 1";
#endif
        }
        for (int i = 0; i < std::max(threads, 1); i++)
        {
            m_workers.push_back(std::make_shared<Tworker>(m_observers));
            m_workers.back()->init(std::forward<_Args>(__args)...);
        }
    }

    std::list<std::shared_ptr<Tworker>> m_workers;

    size_t size() const noexcept override final
    {
        return m_workers.size();
    }

    std::shared_ptr<Tworker> get_parent_worker() const noexcept
    {
        auto it = std::find_if(m_workers.begin(), m_workers.end(),
                               [](auto& worker) { return worker->get_id() == boost::this_thread::get_id(); });
        return it == m_workers.end() ? nullptr : *it;
    }

    std::string get_parent_tag() const noexcept override final
    {
        auto parent = get_parent_worker();
        return nullptr == parent ? "" : parent->get_current_tag();
    }

    const std::vector<std::string> m_observers;

    void wait()
    {
        wait_concrete();
        std::for_each(m_workers.begin(), m_workers.end(), [](auto& worker) { worker->wait(); });
    }
    virtual void wait_concrete() = 0;

    size_t free_workers() const noexcept
    {
        return std::count_if(m_workers.begin(), m_workers.end(), [](const auto& worker) { return worker->isEmpty(); });
    }
};

class ThreadPullSharedQueue : public ThreadPoolT<WorkerSharedQueue>
{
  public:
    ~ThreadPullSharedQueue() override final;
    void terminate() override final;

  private:
    int n_tasks_in_process() noexcept override final;

    void wait_for_one_task_finishing() noexcept override final;

    ThreadPullSharedQueue(const int threads, const std::vector<std::string>& observers);
    friend class ThreadPool;
    void appendFn(const std::function<void()>& fn, const std::string& tag) override final;
    void thread_fn(bool& in_work, std::string& current_tag);
    std::queue<std::pair<std::function<void()>, std::string>> m_fqueue;

    boost::condition_variable m_cv;
    boost::mutex              m_mutex;
    bool                      therminate_pool;

    void wait_concrete() override final;
};

class ThreadPullDifferentQueue : public ThreadPoolT<Worker>
{
  public:
    void terminate() override final;

  private:
    int n_tasks_in_process() noexcept override final;

    void wait_for_one_task_finishing() noexcept override final;

    ThreadPullDifferentQueue(const int threads, const std::vector<std::string>& observers);
    friend class ThreadPool;
    std::shared_ptr<Worker> getFreeWorker();
    boost::mutex            m_mutex;

    void wait_concrete() override final;

    void appendFn(const std::function<void()>& fn, const std::string& tag) override final;
};

} // namespace commtools

#endif // COMM_TOOLS_THREADPOOL_HPP
