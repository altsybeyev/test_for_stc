#ifndef UIUPDATER_H
#define UIUPDATER_H

#include <QObject>

class OperationStatus;

class UIUpdater : public QObject
{
    Q_OBJECT

  public:
    explicit UIUpdater() = default;
  signals:
    void update_status_signal(const OperationStatus&);
    void new_calculation_signal(const int);
    void show_message_signal(const std::string& message, const std::string& type);
    void set_progress_signal(const int n, const int progress);
    void set_result_signal(const int n, const int result);
};

#endif
