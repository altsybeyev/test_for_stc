#ifndef TOOLS_H
#define TOOLS_H

#include <cstdio>

unsigned long get_file_size(std::FILE* file_pointer);

#endif