#include "tools.h"

unsigned long get_file_size(std::FILE* file_pointer)
{
    std::fseek(file_pointer, 0, SEEK_END);
    unsigned long file_size = std::ftell(file_pointer);
    std::rewind(file_pointer);

    return file_size;
}
