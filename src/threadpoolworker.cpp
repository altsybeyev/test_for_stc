#ifndef DONT_USE_BOOSTLOG
#include <common_tools/boostlog.hpp>
#endif

#include "threadpoolworker.h"

namespace commtools
{

Worker::Worker(const std::vector<std::string>& obsevers) noexcept
    : m_terminate(false), m_fqueue(), m_observers(obsevers)
{
}

BaseWorker::~BaseWorker()
{
    m_thread->join();
}
void Worker::init()
{
    base_init(&Worker::thread_fn, this);
}

void Worker::terminate()
{
    m_terminate = true;
    m_cv.notify_one();
    boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
}

Worker::~Worker()
{
    terminate();
}

void Worker::wait()
{
    while (!m_fqueue.empty())
    {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    }
    while (m_in_work)
    {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    }
}

void WorkerSharedQueue::wait()
{
    while (m_in_work)
    {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    }
}

void Worker::appendFn(const std::function<void()>& fn, const std::string& tag)
{
    boost::unique_lock<boost::mutex> locker(m_mutex);
    m_fqueue.push(std::make_pair(fn, tag));
    locker.unlock();
    m_cv.notify_one();
}

size_t Worker::getTaskCount()
{
    boost::unique_lock<boost::mutex> locker(m_mutex);
    return m_fqueue.size();
}

bool Worker::isEmpty()
{
    boost::unique_lock<boost::mutex> locker(m_mutex);
    return m_fqueue.empty();
}

bool WorkerSharedQueue::isEmpty()
{
    return !m_in_work;
}

std::string BaseWorker::get_current_tag()
{
    return current_tag;
}

boost::thread::id BaseWorker::get_id()
{
    return m_thread->get_id();
}

void Worker::thread_fn(bool& in_work, std::string& current_tag_)
{
    while (true)
    {
        boost::unique_lock<boost::mutex> locker(m_mutex);

        m_cv.wait(locker, [&]() { return !m_fqueue.empty() || m_terminate; });

        if (m_terminate && m_fqueue.empty())
        {
            return;
        }

        auto fn_tag = m_fqueue.front();

        locker.unlock();
        current_tag_ = fn_tag.second;

#ifndef DONT_USE_BOOSTLOG
        for (const auto& obs : m_observers)
        {
            BoostLog::get().add_current_thread_filter(current_tag_, obs);
        }
#endif
        in_work = true;

        fn_tag.first();
        in_work = false;

#ifndef DONT_USE_BOOSTLOG
        for (const auto& obs : m_observers)
        {
            BoostLog::get().remove_current_thread_filter(current_tag_, obs);
        }
#endif

        locker.lock();
        m_fqueue.pop();
        locker.unlock();

        current_tag_ = "";
    }
}
} // namespace commtools
