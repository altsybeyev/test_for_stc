#include <iostream>
#include <string>

#include "threadpool.hpp"

#include "fileprocessor.h"
#include "tools.h"
#include "uiupdater.h"

FileProcessor::FileProcessor(const std::shared_ptr<UIUpdater>& updater) noexcept
    : m_updater(updater), m_max_functions(30), m_in_process(false)
{
    unsigned long crc;
    for (int i = 0; i < 256; i++)
    {
        crc = i;
        for (int j = 0; j < 8; j++)
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;
        crc_table[i] = crc;
    };
}

#define TERMINATE_FILE_PROCESS_NOT_CHANGE_STATUS(error_msg)                                                            \
    m_updater->show_message_signal(error_msg, "Error");                                                                \
    return;

#define TERMINATE_FILE_PROCESS(error_msg)                                                                              \
    m_in_process = false;                                                                                              \
    TERMINATE_FILE_PROCESS_NOT_CHANGE_STATUS(error_msg);

#define TERMINATE_FILE_PROCESS_IF_HAS_ERROR(input)                                                                     \
    if (!input.second.empty())                                                                                         \
    {                                                                                                                  \
        TERMINATE_FILE_PROCESS(input.second)                                                                           \
    }

// main file processing function
void FileProcessor::process(const std::string& path, const std::string& n_threads,
                            const std::string& m_block_size) noexcept
{
    if (m_in_process)
    {
        TERMINATE_FILE_PROCESS_NOT_CHANGE_STATUS("Another file processing operation now in progress")
    }
    m_in_process = true;

    // extract and check input data
    const auto input = prepare_input(path, n_threads, m_block_size);
    // terminate process if am error in input data exist
    TERMINATE_FILE_PROCESS_IF_HAS_ERROR(input)
    const auto& input_params = input.first;

    // get and check processing paramters
    const auto process_params = get_process_parameters(input_params);
    // terminate process if an error occured in calculation of processing parameters
    TERMINATE_FILE_PROCESS_IF_HAS_ERROR(process_params)

    // initialization threadpull
    try
    {
        commtools::ThreadPool::get(input_params.n_threads, {}, true);
    }
    catch (const std::exception& e)
    {
        TERMINATE_FILE_PROCESS("Error creation ThreadPool: " + std::string(e.what()))
    }

    // update UI
    m_updater->new_calculation_signal(process_params.first.N);

    // form threads tasks
    const auto possible_errors = make_threads_tasks(input_params, process_params.first);

    // wait calculations finish
    commtools::ThreadPool::get().wait();

    // end of processing
    if (possible_errors.empty())
    {
        m_updater->show_message_signal("Success finish calculations", "Info");
    }
    else
    {
        m_updater->show_message_signal("Calculations finished with errors: " + possible_errors, "Error");
    }
    m_in_process = false;
}

void FileProcessor::process_file_part(const int part, std::shared_ptr<std::vector<char>> data)
{
    // boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    const auto result = CRC32_function(part, data->data(), data->size());
    std::cout << "hash = " << result << std::endl;
    m_updater->set_result_signal(part, result);
}

unsigned int FileProcessor::CRC32_function(const int part, char* buf, unsigned long len)
{

    unsigned long crc = 0xFFFFFFFFUL;

    long progr_step = len / 100;

    const long start_len = len;

    while (len--)
    {
        long cur_progr = start_len - len;

        if (cur_progr % progr_step == 0)
        {
            m_updater->set_progress_signal(part, 100 * cur_progr / start_len);
        }

        crc = crc_table[(crc ^ *buf++) & 0xFF] ^ (crc >> 8);
    }
    m_updater->set_progress_signal(part, 100);

    return crc ^ 0xFFFFFFFFUL;
}

std::pair<FileProcessor::InputData, std::string>
FileProcessor::prepare_input(const std::string& path, const std::string& n_threads, const std::string& m_block_size)
{
    std::pair<InputData, std::string> result;

    std::string input_data_error = "";

    result.first.file_pointer = std::fopen(path.c_str(), "rb");

    if (!result.first.file_pointer)
    {
        result.second += "Error open input file " + path;
    }

    auto try_read_value = [&](const std::string& prefix, const std::string& read_from) -> int {
        int lamba_result;
        try
        {
            lamba_result = std::stoi(read_from);
            if (lamba_result <= 0)
            {
                result.second += prefix + " value <= 0";
            }
        }
        catch (const std::exception& e)
        {
            result.second += prefix;
            result.second += " error conversion to int";
        }
        return lamba_result;
    };

    result.first.n_threads    = try_read_value("\n Error read input n_threads: ", n_threads);
    result.first.m_block_size = try_read_value("\n Error read input m_block_size: ", m_block_size);

    return result;
}

std::pair<FileProcessor::ProcessData, std::string>
FileProcessor::get_process_parameters(const FileProcessor::InputData& input_params) const noexcept
{
    std::pair<ProcessData, std::string> result;

    const auto file_size         = get_file_size(input_params.file_pointer);
    result.first.N               = std::ceil(double(file_size) / input_params.m_block_size);
    result.first.last_block_size = file_size - input_params.m_block_size * (result.first.N - 1);

    if (result.first.N > m_max_functions)
    {
        result.second = "number of functions(" + std::to_string(result.first.N) +
                        ") is greater that max possible number = " + std::to_string(m_max_functions);
    }

    std::cout << "file_size = " << file_size << std::endl;
    std::cout << "N = " << result.first.N << std::endl;
    std::cout << "last_block_size = " << result.first.last_block_size << std::endl;
    std::cout << "input_params.m_block_size = " << input_params.m_block_size << std::endl;

    return result;
}

std::shared_ptr<std::vector<char>> FileProcessor::try_allocate_memory_for_buffer(const long current_block_size) noexcept
{
    auto& thread_pull = commtools::ThreadPool::get();
    // attempt to allocate enough memory for reading M bytes
    size_t i = 0;
    while (true)
    {
        const auto n_tasks_in_process = thread_pull.n_tasks_in_process();
        try
        {
            std::cout << "try allocation buffer for " << current_block_size << std::endl;
            auto ptr = std::make_shared<std::vector<char>>(current_block_size);
            std::cout << "success allocation buffer for " << current_block_size << std::endl;
            return ptr;
        }
        catch (const std::exception& e)
        {
            std::cout << "error try allocation buffer for " << current_block_size << std::endl;

            // if it is impossible to allocate memory and currently there are no calculation we terminate process
            // with error
            if (n_tasks_in_process == 0 && thread_pull.n_tasks_in_process() == 0 && i != 0)
            {
                thread_pull.wait();
                return nullptr;
            }
            // if it is impossible to allocate memory and currently there are no calculation we wait for finishing
            // one task and repeat to allocate

            thread_pull.wait_for_one_task_finishing();
        }
        i++;
    }
}

std::string FileProcessor::make_threads_tasks(const InputData& input_params, const ProcessData& process_params) noexcept
{
    std::string read_errors = "";
    for (int i = 0; i < process_params.N; i++)
    {
        auto current_block_size =
            i == process_params.N - 1 ? process_params.last_block_size : input_params.m_block_size;

        auto buffer = try_allocate_memory_for_buffer(current_block_size);

        if (nullptr == buffer)
        {
            return "Error allocation memory for " + std::to_string(current_block_size) + " bytes";
        }

        const auto readed_bytes =
            std::fread(buffer->data(), sizeof(char), current_block_size, input_params.file_pointer);

        if (readed_bytes != current_block_size)
        {
            read_errors +=
                "\nError read " + std::to_string(current_block_size) + " bytes from part " + std::to_string(i);
        }
        else
        {
            commtools::ThreadPool::get().run_async_master("", &FileProcessor::process_file_part, this, i, buffer);
        }
    }
    return read_errors;
}
