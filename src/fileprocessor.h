
#ifndef FILE_PROCESSOR_H
#define FILE_PROCESSOR_H

#include <memory>

class UIUpdater;
class FileProcessor

{
  public:
    FileProcessor(const std::shared_ptr<UIUpdater>& updater) noexcept;

    // main file processing function
    void process(const std::string& path, const std::string& n_threads, const std::string& m_block_size) noexcept;

  private:
    // helper structs
    struct InputData
    {
        int        n_threads;
        long       m_block_size;
        std::FILE* file_pointer;
    };

    struct ProcessData
    {
        int  N;
        long last_block_size;
    };

    std::shared_ptr<UIUpdater> m_updater; // observer of calculation status

    const int m_max_functions; // how many hash functions we may calculate for one file

    bool m_in_process; // status. if true the calculation now already in progress

    // check and prepare input parameters
    static std::pair<InputData, std::string> prepare_input(const std::string& path, const std::string& n_threads,
                                                           const std::string& m_block_size);

    // check and prepare input parameters
    std::pair<ProcessData, std::string> get_process_parameters(const InputData& input_params) const noexcept;

    // calculation of hash for file part
    void process_file_part(const int part, std::shared_ptr<std::vector<char>> data);

    // allocation memory for reading file part
    static std::shared_ptr<std::vector<char>> try_allocate_memory_for_buffer(const long current_block_size) noexcept;

    // preparation tasks for threads
    std::string make_threads_tasks(const InputData& input_params, const ProcessData& process_params) noexcept;

    unsigned int CRC32_function(const int part, char* buf, unsigned long len);

    unsigned long crc_table[256];
};

#endif
