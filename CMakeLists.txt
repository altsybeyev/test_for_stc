cmake_minimum_required(VERSION 2.8)
project(test_for_sct)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
set(CMAKE_CXX_FLAGS "-std=c++14")
endif()

if(CMAKE_BUILD_TYPE MATCHES Debug)
    if(MSVC)
        if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
            string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
        else()
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
        endif()
    elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g3 -Wall -Wno-long-long -pedantic")
    endif()
else()
    if(MSVC)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W0 /Ox")
    elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g0 -Os")
    endif()
endif()

add_definitions(-DBOOST_SP_USE_SPINLOCK -DBOOST_SP_USE_PTHREADS -DDONT_USE_BOOSTLOG -DBOOST_THREAD_PROVIDES_FUTURE)

include_directories (.)

set(Boost_USE_STATIC_LIBS Off)
set(Boost_USE_MULTITHREAD On)
set(Boost_USE_STATIC_RUNTIME Off)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_AUTOMOC ON) 

find_package(Boost 1.55 COMPONENTS thread system  REQUIRED)

find_package(Qt5Widgets REQUIRED)

include_directories (${Boost_INCLUDE_DIRS})
include_directories (${Qt5Widgets_INCLUDE_DIRS})

aux_source_directory(src SRC)

add_executable(${PROJECT_NAME} 
               ${SRC}
              )

target_link_libraries(${PROJECT_NAME}
                      ${Boost_LIBRARIES}
                      Qt5::Widgets 
                      pthread 
                     )